const fs = require('fs')
const massiveDownload = require('../index.js')

async function main() {

	console.log(' -- Start --')

	// Use main module
	var result = await massiveDownload({
		apiKey: 'AIzaSyDCE5dwZl5SfcscHKYTSi5B3dPDd-sL0Gc',
		outputPath: './test/downloaded',
		verbose: true,
		searchTerms: fs.readFileSync('test/toDownload.txt').toString().split('\r\n')
	})

	console.log(' -- Finished --')
	console.log('Output:')
	console.log(result)


}
main()
