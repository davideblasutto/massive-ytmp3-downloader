# massive-ytmp3-downloader

Given an array of search terms, this module searches them on YouTube and downloads straight to the file system the first result's MP3 track of each search term.

## Usage
This module exports a single `async` function.

Since in does searches on YouTube, you'll need an API key.

### Example
```javascript
const massiveDownload = require('../index.js')
await massiveDownload({
	apiKey: 'YOUR GOOGLE/YOUTUBE API KEY',
	outputPath: './downloaded',
	verbose: true,
	searchTerms: [
		'Dan Lebowitz - Say What',
		'Anno Domini Beats - Stand',
		'poV04HL5Hhg' // You can directly use the video ID
	]
})
```
