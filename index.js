// This module searches on YouTube a list of search terms and downloads on the file system the first result's MP3 track of each search key

const youtubeSearch = require ('youtube-search-promise')
const fs = require('fs')
const path = require('path')
const sanitize = require('sanitize-filename')

const Downloader = require('./downloader.js')

module.exports = async function download(parameters) {

	var generalOutput = []

	for ([i, searchTerm] of parameters.searchTerms.entries()) {

		// Skip empty search term
		if (!searchTerm)
			continue

		if (parameters.verbose) {
			console.log('')
			console.log('Search ' + (i + 1) + ' of ' + parameters.searchTerms.length)
			console.log('Finding "' + searchTerm + '" on YouTube...')
		}

		// Search on YouTube
		var results = await youtubeSearch(
			searchTerm,
			{
				maxResults: 1,
				key: parameters.apiKey
			}
		)

		if (!results) {
			// No results (I thinks it's almost impossible)
			if (parameters.verbose)
				console.log('No result')
			continue
		}

		if (parameters.verbose)
			console.log('Found video: ID ' + results[0].id)

		// Check if the file has been already downloded
		var outputFileName = sanitize(results[0].title + '.mp3')
		if (fs.existsSync(path.join(parameters.outputPath, outputFileName))) {

			// Already exists
			if (parameters.verbose)
				console.log('File "' + outputFileName + '" already exists')

			generalOutput.push({
				id: results[0].id,
				file: path.join(parameters.outputPath, outputFileName),
				status: 'already exits',
				more: {
					parameters: parameters,
					searchResults: results,
				}
			})

			continue

		}

		// Download the audio file
		if (parameters.verbose)
			console.log('Downloading file...')

		var downloader = new Downloader({
			outputPath: parameters.outputPath,
			verbose: parameters.verbose,
			showProgress: parameters.verbose
		})

		var downloadOutput = await downloader.download(results[0].id, outputFileName)

		// Done
		if (parameters.verbose)
			console.log('File downloaded: ' + downloadOutput.file)

		// Output
		generalOutput.push({
			id: results[0].id,
			file: downloadOutput.file,
			status: 'downloaded',
			more: {
				parameters: parameters,
				searchResults: results,
				downloadOutput: downloadOutput
			}
		})

	}

	return generalOutput

}
