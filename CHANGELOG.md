# Version 1.1.1 (18/08/2018)

* Better logging and function output

# Version 1.1.0 (08/08/2018)

* Now the function returns YouTube IDs, local files and other stuff

# Version 1.0.2 (17/07/2018)

* Better `README.md`

# Version 1.0.1 (11/07/2018)

* Bugfix: downloader.js used wrong property name

# Version 1.0.0

* First release
