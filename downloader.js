// This module allows to download a single MP3 track from YouTube, given the video ID, straight to the file system
// It's inspired by youtube-mp3-downloader's readme

const ffmpeg = require('ffmpeg-static')
const YoutubeMp3Downloader = require('youtube-mp3-downloader')

var Downloader = function(parameters) {

	this.parameters = parameters

}

Downloader.prototype.download = function(idVideo, destination) {

	return new Promise((resolve, reject) => {

		var self = this

		// Configure YoutubeMp3Downloader with your settings
		var YD = new YoutubeMp3Downloader({
			ffmpegPath: ffmpeg.path,
			outputPath: this.parameters.outputPath,
			youtubeVideoQuality: 'highest',
			queueParallelism: 2,
			progressTimeout: 1000
		})

		// Download video and save as MP3 file
		if (self.parameters.verbose)
			console.log('[Downloader] Downloading track...')
		YD.download(idVideo, destination)

		YD.on('finished', function(error, data) {
			if (self.parameters.verbose)
				console.log('[Downloader] Track downloaded')
			resolve(data)
		})

		YD.on('error', function(error) {
			if (self.parameters.verbose)
				console.log('[Downloader] Error while downloading track:', error)
			reject(error)
		})

		YD.on('progress', function(progress) {
			if (self.parameters.showProgress)
				process.stdout.write('[Downloader] ' + (progress.progress.percentage).toFixed(2) + '%    \r')
		})

	})

}

module.exports = Downloader
